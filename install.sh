#!/bin/sh

if [ -d ~/.vim ]
then
    echo "You have own .vim directory"
    exit 1
fi

if [ ! -e ~/.vim/autoload/plug.vim ]
then
    echo "Install vim-plug"
    curl -fLo ~/.vim/autoload/plug.vim --create-dirs \
        https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
fi

if [ ! -e ~/.vimrc -o -L ~/.vimrc ]
then
    echo "Set symlink to vimrc"
    ln -sf `pwd`/vimrc ~/.vimrc
else
    echo "You have own vimrc script"
    exit 1
fi

vim -c "PlugInstall | qa"
