" Initialize with sane defaults
set nocompatible

set shortmess=I
set cindent
set hidden
set number
set numberwidth=6
set signcolumn=yes
set nobackup
set backupcopy=yes
set incsearch
set autoindent
set backspace=indent,eol,start
set complete-=i

set smarttab
set expandtab
set sw=4 ts=4 sts=4

set ruler
set wildmenu
if v:version >= 900
set wildoptions=fuzzy,pum
set fillchars+=eob:\ ,diff:-
endif

set scrolloff=3
set encoding=utf-8
set mouse=nvi
set ttymouse=xterm2
set path=**

set backupdir=/tmp//
set directory=/tmp//
set undodir=/tmp//

set laststatus=2
" set showtabline=2
set clipboard=unnamedplus

if !has('gui_running') && &term =~ '^\%(screen\|tmux\)'
    let &t_8f = "\<Esc>[38;2;%lu;%lu;%lum"
    let &t_8b = "\<Esc>[48;2;%lu;%lu;%lum"
endif

let g:utl_cfg_hdl_scm_http_system = "silent !xdg-open '%u#%f'"

" Install plugins

call plug#begin()

Plug 'NLKNguyen/papercolor-theme'
Plug 'ghifarit53/tokyonight-vim'
Plug 'ericbn/vim-solarized'
Plug 'tpope/vim-repeat'
Plug 'tpope/vim-surround'
Plug 'tpope/vim-vinegar'
Plug 'lambdalisue/nerdfont.vim'

Plug 'liuchengxu/vim-which-key'

Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
Plug 'junegunn/fzf.vim'
Plug 'lambdalisue/fern.vim'
Plug 'lambdalisue/fern-renderer-nerdfont.vim'

Plug 'tpope/vim-fugitive'
Plug 'tpope/vim-rhubarb'
Plug 'stsewd/fzf-checkout.vim'

Plug 'prabirshrestha/vim-lsp'
Plug 'mattn/vim-lsp-settings'
Plug 'prabirshrestha/asyncomplete.vim'
Plug 'prabirshrestha/asyncomplete-lsp.vim'
Plug 'hrsh7th/vim-vsnip'
Plug 'hrsh7th/vim-vsnip-integ'

Plug 'itchyny/lightline.vim'
" Plug 'mengelbrecht/lightline-bufferline'
Plug 'micchy326/lightline-lsp-progress'

Plug 'ntpeters/vim-better-whitespace'

Plug 'psf/black', { 'branch': 'stable' }

Plug 'tpope/vim-projectionist'
Plug 'airblade/vim-rooter'

Plug 'guns/vim-sexp'
Plug 'tpope/vim-sexp-mappings-for-regular-people'
Plug 'clojure-vim/clojure.vim'
Plug 'tpope/vim-fireplace'

Plug 'tpope/vim-dispatch'
Plug 'clojure-vim/vim-jack-in'

Plug 'jpalardy/vim-slime'

Plug 'jceb/vim-orgmode'
Plug 'tpope/vim-speeddating'
Plug 'vim-scripts/utl.vim'
Plug 'dhruvasagar/vim-table-mode'

Plug 'madox2/vim-ai', { 'do': './install.sh' }

Plug 'nanotee/zoxide.vim'

Plug 'vim-scripts/Improved-AnsiEsc'
Plug 'https://mrroman.pl/git/mrroman/clojure-test.vim.git'

Plug 'junkblocker/patchreview-vim'

call plug#end()

" Setup colors
set termguicolors
set background=dark
colorscheme tokyonight
highlight lspReference ctermfg=brown guifg=brown ctermbg=lightgrey guibg=lightgray

" White space and indent
filetype plugin indent on

let g:better_whitespace_enabled=1
let g:strip_whitespace_on_save=1
let g:strip_whitespace_confirm=0
let g:strip_whitelines_at_eof=1

" Fern
"
let g:fern#renderer = "nerdfont"

augroup FernGroup
    autocmd!
    autocmd FileType fern setlocal norelativenumber | setlocal nonumber
augroup END

" Lightline
"

let g:lightline = {
      \ 'colorscheme': 'tokyonight',
      \ 'active': {
      \   'left': [ [ 'mode', 'paste' ],
      \             [ 'gitbranch', 'readonly', 'cwd', 'filename', 'modified', 'lsp_status' ] ]
      \ },
      \ 'component_function': {
      \   'gitbranch': 'FugitiveHead',
      \   'lsp_status': 'lightline_lsp_progress#progress',
      \   'cwd': 'getcwd'
      \ }
      \ }

"      \ 'tabline': {
"      \   'left': [ ['buffers'] ],
"      \   'right': [ ['close'] ]
"      \ },
"      \ 'component_expand': {
"      \   'buffers': 'lightline#bufferline#buffers'
"      \ },
"      \ 'component_type': {
"      \   'buffers': 'tabsel'
"      \ },
"
autocmd BufWritePost,TextChanged,TextChangedI * call lightline#update()

" let g:lightline#bufferline#enable_nerdfont = 1


" nmap <silent>H <Plug>lightline#bufferline#go_previous()
" nmap <silent>L <Plug>lightline#bufferline#go_next()


" Mappings

let g:mapleader="\<Space>"
let g:maplocalleader=','

" Whichkey
"

let g:which_key_map = {}
set timeoutlen=500

nnoremap <silent> <leader> :WhichKey '<Space>'<CR>
call which_key#register('<Space>', "g:which_key_map")

" LSP

" let g:lsp_use_native_client = 1
let g:lsp_code_action_ui = 'float'
let g:lsp_work_done_progress_enabled = 1
let g:lsp_diagnostics_virtual_text_align = 'right'
let g:lsp_diagnostics_virtual_text_insert_mode_enabled = 0
let g:lsp_document_code_action_signs_enabled = 0

function! MyGetSupportedCapabilities(server_info) abort
    let l:capabilities = lsp#default_get_supported_capabilities(a:server_info)
    let l:capabilities.workspace.workspaceEdit = {}

    return l:capabilities
endfunction

function! s:on_lsp_setup()
    let g:lsp_get_supported_capabilities = [function('MyGetSupportedCapabilities')]
endfunction

function! s:on_lsp_buffer_enabled() abort
    setlocal omnifunc=lsp#complete
    setlocal signcolumn=yes
    if exists('+tagfunc') | setlocal tagfunc=lsp#tagfunc | endif

    let g:which_key_map.l = {'name': '+lsp'}
    nmap <buffer> gd <plug>(lsp-definition)
    nmap <buffer> gs <plug>(lsp-document-symbol-search)
    nmap <buffer> gS <plug>(lsp-workspace-symbol-search)
    nmap <buffer> gr <plug>(lsp-references)
    nmap <buffer> gi <plug>(lsp-implementation)
    nmap <buffer> gt <plug>(lsp-type-definition)
    let g:which_key_map.l.a = 'code-actions'
    nmap <buffer> <leader>la <plug>(lsp-code-action)
    vmap <buffer> <leader>la <plug>(lsp-code-action)
    let g:which_key_map.l.r = 'rename'
    nmap <buffer> <leader>lr <plug>(lsp-rename)
    let g:which_key_map.l.o = 'organize-imports'
    nmap <buffer> <leader>lo :LspCodeActionSync source.organizeImports<CR>
    nmap <buffer> [g <plug>(lsp-previous-diagnostic)
    nmap <buffer> ]g <plug>(lsp-next-diagnostic)
    nmap <buffer> K <plug>(lsp-hover)
    nnoremap <buffer> <expr><c-f> lsp#scroll(+4)
    nnoremap <buffer> <expr><c-d> lsp#scroll(-4)

    let g:lsp_format_sync_timeout = 1000
    autocmd! BufWritePre *.rs,*.go,*.clj call execute('LspDocumentFormatSync')

    " refer to doc to add more commands
endfunction

augroup lsp_install
    au!
    autocmd User lsp_setup call s:on_lsp_setup()
    " call s:on_lsp_buffer_enabled only for languages that has the server registered.
    autocmd User lsp_buffer_enabled call s:on_lsp_buffer_enabled()
augroup END

let g:lsp_diagnostics_echo_cursor = 1
let g:lsp_experimental_workspace_folders = 1

let g:lsp_settings = {
            \   'clojure-lsp': {
            \     'root_uri_patterns': ['deps.edn', 'project.clj' ]
            \   }
            \ }
let g:lsp_settings_filetype_python = ['pyright-langserver']

" VIM Slime
let g:slime_target='vimterminal'
let g:slime_vimterminal_config = {"term_finish": "close", "vertical": 1}
let g:slime_no_mappings = 1

xmap <silent> <leader>s <Plug>SlimeRegionSend
nmap <silent> <leader>sm <Plug>SlimeMotionSend
nmap <silent> <leader>ss <Plug>SlimeLineSend

function! s:clojure_settings()
    let g:which_key_map.r = { 'name': '+run' }
    nmap <silent> <leader>rr :Clj! -Adev:test<CR>
    let g:which_key_map.r.r = 'jack-in'
    let g:which_key_map.r.t = { 'name': '+tests' }
    let g:which_key_map.r.t.t = 'run closest test'
    nmap <silent> <leader>rtt :execute "Make % -test-line " . (line(".") + 1)<CR>
    let g:which_key_map.r.t.n = 'run ns tests'
    nmap <silent> <leader>rtn :Make %<CR>
    let g:which_key_map.r.t.a = 'run all test'
    nmap <silent> <leader>rta :Make<CR>
    compiler clojure-test
endfunction

let g:clojure_align_multiline_strings=1
let g:clojure_align_subforms=1
let g:clojure_fuzzy_indent = 1
let g:clojure_fuzzy_indent_patterns = ['^with', '^def', '^let']
let g:clojure_fuzzy_indent_blacklist = ['-fn$', '\v^with-%(meta|out-str|loading-context)$']

autocmd FileType clojure call s:clojure_settings()

nmap <silent> <leader>w :w<CR>
let g:which_key_map.w = 'save-buffer'

noremap <silent> <C-S>  :update<CR>
vnoremap <silent> <C-S> <C-C>:update<CR>

nnoremap Q q
nnoremap q <Nop>

nnoremap gu <Nop>
nnoremap gU <Nop>

let g:which_key_map.q = { 'name': '+quit' }
nmap <silent> <leader>qq :q<CR>
let g:which_key_map.q.q = 'quit'
nmap <silent> <leader>qa :qa<CR>
let g:which_key_map.q.a = 'quit-all'
nmap <silent> <leader>qA :qa!<CR>
let g:which_key_map.q.A = 'quit-all!'

nmap <silent> <leader><leader> :History<CR>
let g:which_key_map[' '] = 'recent-files'
nmap <silent> <leader>f :Files<CR>
let g:which_key_map.f = 'root-files'
nmap <silent> <leader>p :Zi<CR>
let g:which_key_map.p = 'projects'
nmap <silent> <leader><tab> :Fern . -reveal=% -drawer -toggle<CR>
let g:which_key_map['<Tab>'] = 'file-tree'

nmap <silent> <leader>, :Buffers<CR>
let g:which_key_map[','] = 'buffers'
nmap <silent> <leader>bd :bd<CR>
let g:which_key_map.b = { 'name': '+buffers', 'd': 'delete' }
nmap <silent> <leader>/ :Rg<CR>
let g:which_key_map['/'] = 'search-project'

let g:which_key_map.g = { 'name': '+git' }
nmap <silent> <leader>gg :G<CR>
let g:which_key_map.g.g = 'status'
nmap <silent> <leader>gb :Git blame<CR>
let g:which_key_map.g.b = 'blame'
nmap <silent> <leader>gc :GBranches<CR>
let g:which_key_map.g.c = 'branches'
nmap <silent> <leader>gP :G push -u origin HEAD<CR>
let g:which_key_map.g.P = 'push'
nmap <silent> <leader>gf :G pull<CR>
let g:which_key_map.g.f = 'pull'
nmap <silent> <leader>gF :G fetch<CR>
let g:which_key_map.g.F = 'fetch'
nmap <silent> <leader>gl :ter ++close lazygit<CR>
let g:which_key_map.g.l = 'lazy-git'

let g:which_key_map.h = {'name': '+help'}
nmap <silent> <leader>hm :Maps<CR>
let g:which_key_map.h.m = 'maps'

inoremap <expr><C-J> pumvisible() ? "\<C-n>" : "\<C-J>"
inoremap <expr><C-K> pumvisible() ? "\<C-p>" : "\<C-K>"

" More granular vim undo
inoremap <c-u> <c-g>u<c-u>
inoremap <c-w> <c-g>u<c-w>
inoremap <End> <C-g>u<End>
inoremap <BS> <c-g>u<BS>
inoremap <CR> <c-g>u<CR>
inoremap <del> <c-g>u<del>

" Terminal keys
"

tnoremap <C-PageUp> <C-W>:tabprev<CR>
tnoremap <C-PageDown> <C-W>:tabnext<CR>

" Vsnip
"

" Jump forward or backward
imap <expr> <Tab>   vsnip#jumpable(1)   ? '<Plug>(vsnip-jump-next)'      : '<Tab>'
smap <expr> <Tab>   vsnip#jumpable(1)   ? '<Plug>(vsnip-jump-next)'      : '<Tab>'
imap <expr> <S-Tab> vsnip#jumpable(-1)  ? '<Plug>(vsnip-jump-prev)'      : '<S-Tab>'
smap <expr> <S-Tab> vsnip#jumpable(-1)  ? '<Plug>(vsnip-jump-prev)'      : '<S-Tab>'


" Autocomplete
"

inoremap <expr> <cr>    pumvisible() ? asyncomplete#close_popup() : "\<cr>"

" AI
"
let initial_prompt =<< trim END
>>> system

You are going to play a role of a completion engine with following parameters:
Task: Provide compact code/text completion, generation, transformation or explanation
Topic: general programming and text editing
Style: Plain result without any commentary, unless commentary is necessary
Audience: Users of text editor and programmers that need to transform/generate text
END

let chat_engine_config = {
            \  "engine": "chat",
            \  "options": {
            \    "model": "gpt-3.5-turbo",
            \    "max_tokens": 1000,
            \    "temperature": 0.1,
            \    "request_timeout": 20,
            \    "selection_boundary": "",
            \    "initial_prompt": initial_prompt,
            \  },
            \}

let g:vim_ai_complete = chat_engine_config
let g:vim_ai_edit = chat_engine_config

" vim-rooter
"

let g:rooter_patterns = ['deps.edn', 'project.clj', 'pyproject.toml', '.git']
let g:rooter_cd_cmd = 'lcd'
let g:rooter_silent_chdir = 1

" Projectionist
"

let g:projectionist_heuristics = {
            \   "deps.edn": {
            \     "src/*.clj": {"type": "source", "alternate": "test/{}_test.clj" },
            \     "test/*_test.clj": {"type": "test", "alternate": "src/{}.clj" }
            \   },
            \   "project.clj": {
            \     "src/*.clj": {"type": "source", "alternate": "test/{}_test.clj" },
            \     "test/*_test.clj": {"type": "test", "alternate": "src/{}.clj" }
            \   }
            \ }

nmap <silent> ga :A<CR>

" Vim-Org
"

let g:org_todo_keywords = ['TODO', 'ONGOING', 'DONE']

" fugitive
"

autocmd User FugitiveEditor :setlocal spell spelllang=en_us
